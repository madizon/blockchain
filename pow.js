const SHA256 = require('crypto-js/sha256');

class Block{
    constructor(index, data, previousHash=""){
     this.index = index;
     this.timestamp = new Date().toLocaleDateString("en-US");
     this.data = data;
     this.previousHash = previousHash;
     this.hash = this.getHash();    
     this.nonce = 0; //number only used once, where the blockchain miners are solving for 
    }

    getHash(){
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce).toString();
    }   

    mineBlock(difficulty){
        while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")){
            this.nonce++;
            this.hash = this.getHash();
        }

        console.log("Block mined: " + this.hash)
    }
}


class BlockChain{
    constructor(){
        this.chain = [this.getGenesisBlock()];   
        this.difficulty = 10;  
    }

    getGenesisBlock(){
        return new Block(0, "09/07/2020", "Initial Block in the Chain");
    }

    getLatestBlock(){
        return this.chain[this.chain.length - 1];
    }

    addNewBlock(newBlock){
        newBlock.previousHash = this.getLatestBlock().hash;
        newBlock.mineBlock(this.difficulty);        
        this.chain.push(newBlock);
    }

    isValidChain()
    {
      for (let index = 1; index < this.chain.length; index++) {
          const currentBlock = this.chain[index];
          const previousBlock = this.chain[index - 1];

          if(currentBlock.hash !== currentBlock.getHash()){
              return false;
          }

          if(currentBlock.previousHash !== previousBlock.hash){
              return false;
          }

          return true;
      }
    }
}

let blockChain = new BlockChain();
console.log('Mining 1...')
blockChain.addNewBlock(new Block(1, {sender: "Employee 1", recipient: "Employee 2", quantity: 50}));

console.log('Mining 2...')
blockChain.addNewBlock(new Block(2, {sender: "Employee 2", recipient: "Employee 3", quantity: 10}));
