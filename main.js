const SHA256 = require('crypto-js/sha256');

class Block{
    constructor(index, data, previousHash=""){
     this.index = index;
     this.timestamp = new Date().toLocaleDateString("en-US");
     this.data = data;
     this.previousHash = previousHash;
     this.hash = this.getHash();    
    }

    getHash(){
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data)).toString();
    }   
}


class BlockChain{
    constructor(){
        this.chain = [this.getGenesisBlock()];   
    }

    getGenesisBlock(){
        return new Block(0, "Initial Block in the Chain");
    }

    getLatestBlock(){
        return this.chain[this.chain.length - 1];
    }

    addNewBlock(newBlock){
        newBlock.previousHash = this.getLatestBlock().hash;
        newBlock.hash = newBlock.getHash();        
        this.chain.push(newBlock);
    }

    isValidChain()
    {
      for (let index = 1; index < this.chain.length; index++) {
          const currentBlock = this.chain[index];
          const previousBlock = this.chain[index - 1];

          if(currentBlock.hash !== currentBlock.getHash()){
              return false;
          }

          if(currentBlock.previousHash !== previousBlock.hash){
              return false;
          }

          return true;
      }
    }
}

let blockChain = new BlockChain();
blockChain.addNewBlock(new Block(1, {sender: "Employee 1", receiver: "Employee 2", amount: 50}));
blockChain.addNewBlock(new Block(2, {sender: "Employee 2", receiver: "Employee 3", amount: 10}));
blockChain.addNewBlock(new Block(3, {sender: "Employee 4",  receiver: "Employee 1", amount: 200}));
console.log(JSON.stringify(blockChain, null, 4));

console.log('BEFORE UPDATE: Is blockchain valid: ' + blockChain.isValidChain());
blockChain.chain[1].data.sender = 'Employee 3';
console.log('AFTER UPDATE: Is blockchain valid: ' + blockChain.isValidChain());



